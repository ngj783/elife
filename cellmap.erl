-module(cellmap).

-export([new/2, set_cell/3, clear_cell/3, cell_state/3, count_neighbors/3,
    next_generation/1]).

-record(cellmap,
    {height,
     width,
     cells = gb_trees:empty()
    }).

%% API

new(Height, Width) ->
    #cellmap{height = Height, width = Width}.

set_cell(I, J, #cellmap{cells = Cells} = Cellmap) ->
    Key = key(I, J),
    case gb_trees:lookup(Key, Cells) of
        {value, <<_NeighborCount:4, 1:1, _Changed:1>>} ->
            error_logger:error_msg("Cellmap is not changed: ~w\n", [Cells]),
            Cellmap;
        {value, <<NeighborCount:4, 0:1, _Changed:1>>} ->
            NewCells = gb_trees:enter(Key, <<NeighborCount:4, 1:1, 1:1>>,
                handle_neighbor_birth(I, J, Cellmap)),
            error_logger:error_msg("Cellmap: ~w\n", [NewCells]),
            Cellmap#cellmap{cells = NewCells};
        none ->
            NewCells = gb_trees:insert(Key, <<0:4, 1:1, 1:1>>,
                handle_neighbor_birth(I, J, Cellmap)),
            error_logger:error_msg("Cellmap: ~w\n", [NewCells]),
            Cellmap#cellmap{cells = NewCells}
    end.

clear_cell(I, J, #cellmap{cells = Cells} = Cellmap) ->
    Key = key(I, J),
    case gb_trees:lookup(Key, Cells) of
        none ->
            error_logger:error_msg("Cellmap is not changed: ~w\n", [Cells]),
            Cellmap;
        {value, <<_NeighborCount:4, 0:1, _Changed:1>>} ->
            error_logger:error_msg("Cellmap is not changed: ~w\n", [Cells]),
            Cellmap;
        {value, <<NeighborCount:4, 1:1, _Changed:1>>} ->
            NewCells = gb_trees:enter(Key, <<NeighborCount:4, 0:1, 1:1>>,
                handle_neighbor_death(I, J, Cellmap)),
            error_logger:error_msg("Cellmap: ~w\n", [NewCells]),
            Cellmap#cellmap{cells = NewCells}
    end.

cell_state(I, J, #cellmap{cells = Cells}) ->
    case gb_trees:lookup(key(I, J), Cells) of
        {value, <<_NeighborCount:4, CellState:1, _Changed:1>>} -> CellState;
        none -> 0
    end.

count_neighbors(I, J, #cellmap{cells = Cells}) ->
    case gb_trees:lookup(key(I, J), Cells) of
        {value, <<NeighborCount:4, _CellState:1, _Changed:1>>} ->
            NeighborCount;
        none -> 0
    end.

next_generation(#cellmap{cells = Cells} = Cellmap) ->
    UnfilteredCellmap = #cellmap{cells = UnfilteredCells} =
        next_generation(gb_trees:iterator(Cells), Cellmap),
    Iter = gb_trees:iterator(UnfilteredCells),
    delete_dead_unchanged_alone_cells(Iter, UnfilteredCellmap).

%% Internal

next_generation(none, Cellmap) ->
    Cellmap;
next_generation({[I, J], <<3:4, 0:1, _Changed:1>>, Iter}, Cellmap) ->
    NewCellmap = set_cell(I, J, Cellmap),
    next_generation(gb_trees:next(Iter), NewCellmap);
next_generation({[I, J], <<NeighborCount:4, 1:1, _Changed:1>>, Iter},
    Cellmap) when NeighborCount < 2; NeighborCount > 3 ->
    NewCellmap = clear_cell(I, J, Cellmap),
    next_generation(gb_trees:next(Iter), NewCellmap);
next_generation({Key, <<NeighborCount:4, CellState:1, 1:1>>, Iter},
    #cellmap{cells = Cells} = Cellmap) ->
    NewCellmap = Cellmap#cellmap{cells =
        gb_trees:enter(Key, <<NeighborCount:4, CellState:1, 0:1>>, Cells)},
    next_generation(gb_trees:next(Iter), NewCellmap);
next_generation({_Key, _Val, Iter}, Cellmap) ->
    next_generation(gb_trees:next(Iter), Cellmap).

delete_dead_unchanged_alone_cells(none, Cellmap) ->
    Cellmap;
delete_dead_unchanged_alone_cells({Key, <<0:6>>, Iter},
    #cellmap{cells = Cells} = Cellmap) ->
    NewCellmap = Cellmap#cellmap{cells = gb_trees:delete(Key, Cells)},
    delete_dead_unchanged_alone_cells(gb_trees:next(Iter), NewCellmap);
delete_dead_unchanged_alone_cells({_Key, _Val, Iter}, Cellmap) ->
    delete_dead_unchanged_alone_cells(gb_trees:next(Iter), Cellmap).

neighbors_indexes(I, J, Height, Width) ->
    [[A, B] || A <- valid_indexes(I, Height), B <- valid_indexes(J, Width)]
        -- [[I, J]].

valid_indexes(0, Length) ->
    [Length - 1, 0, 1];
valid_indexes(X, Length) when X =:= Length ->
    [X-1, X, 0];
valid_indexes(X, _Length) ->
    [X-1, X, X+1].

key(I, J) ->
    [I, J].

handle_neighbor_birth(I, J,
    #cellmap{height = Height, width = Width, cells = Cells}) ->
    NeighborsIndexes = neighbors_indexes(I, J, Height, Width),
    lists:foldl(fun([NI, NJ], NCells) ->
                    increment_neighbor_count(NI, NJ, NCells)
                end, Cells, NeighborsIndexes).

handle_neighbor_death(I, J,
    #cellmap{height = Height, width = Width, cells = Cells}) ->
    NeighborsIndexes = neighbors_indexes(I, J, Height, Width),
    lists:foldl(fun([NI, NJ], NCells) ->
                    decrement_neighbor_count(NI, NJ, NCells)
                end, Cells, NeighborsIndexes).

increment_neighbor_count(I, J, Cells) ->
    Key = key(I, J),
    case gb_trees:lookup(Key, Cells) of
        {value, <<NeighborCount:4, CellState:1, Changed:1>>} ->
            NewNeighborCount = NeighborCount + 1,
            gb_trees:enter(Key, <<NewNeighborCount:4, CellState:1, Changed:1>>,
                Cells);
        none ->
            gb_trees:insert(Key, <<1:4, 0:1, 0:1>>, Cells)
    end.

decrement_neighbor_count(I, J, Cells) ->
    Key = key(I, J),
    <<NeighborCount:4, CellState:1, Changed:1>> = gb_trees:get(Key, Cells),
    NewNeighborCount = NeighborCount - 1,
    gb_trees:enter(Key, <<NewNeighborCount:4, CellState:1, Changed:1>>, Cells).
