-module(el_grid).

-behaviour(wx_object).

%% Client API
-export([start/3]).

%% wx_object callbacks
-export([init/1, terminate/2,  code_change/3,
    handle_info/2, handle_call/3, handle_cast/2, handle_event/2]).

-include_lib("wx/include/wx.hrl").

-define(MIN_CELL_SIZE, 15).

-record(state,
    {
        parent,
        grid,
        cellmap
    }).

start(Parent, NumRows, NumCols) ->
    wx_object:start_link(?MODULE, [Parent, NumRows, NumCols], []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
init([Parent, NumRows, NumCols]) ->
    wx:batch(fun() -> do_init([Parent, NumRows, NumCols]) end).

do_init([Parent, NumRows, NumCols]) ->
    Panel = wxPanel:new(Parent, []),

    %% Setup sizers
    MainSizer = wxBoxSizer:new(?wxVERTICAL),
    ButtonSizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, [{label, "Button"}]),
    GridSizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, [{label, "Grid"}]),

    Button = wxButton:new(Panel, 3, [{label, "Next"}, {size, {50, 20}}]),

    Grid = create_grid(Panel, NumRows, NumCols),

    %% Add to sizers
    wxSizer:add(ButtonSizer, Button, [{proportion, 0}]),
    wxSizer:add(MainSizer, ButtonSizer, [{proportion, 0}]),

    wxSizer:add(GridSizer, Grid, [{proportion, 0}]),
    % Options = [{flag, ?wxEXPAND}, {proportion, 1}],
    wxSizer:add(MainSizer, GridSizer, []),

    wxPanel:setSizer(Panel, MainSizer),

    Cellmap = cellmap:new(NumRows, NumCols),
    {Panel, #state{parent = Panel, grid = Grid, cellmap = Cellmap}}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Async Events are handled in handle_event as in handle_info
handle_event(#wx{event = #wxGrid{
  type = grid_cell_left_click, row = Row, col = Col}},
  State = #state{grid = Grid, cellmap = Cellmap}) ->
    case wxGrid:getCellBackgroundColour(Grid, Row, Col) of
        {255,255,255,255} ->
            NewCellmap = cellmap:set_cell(Row, Col, Cellmap),
            wxGrid:setCellBackgroundColour(Grid, Row, Col, ?wxBLACK),
            error_logger:info_msg("Colour of the cell {~p, ~p} "
                "has changed to black.\n", [Row, Col]);
        {0,0,0,255} ->
            NewCellmap = cellmap:clear_cell(Row, Col, Cellmap),
            wxGrid:setCellBackgroundColour(Grid, Row, Col, ?wxWHITE),
            error_logger:info_msg("Colour of the cell {~p, ~p} "
                "has changed to white.\n", [Row, Col]);
        Colour ->
            NewCellmap = Cellmap,
            error_logger:error_msg("Invalid cell colour: ~w\n", [Colour])
    end,
    wxGrid:refresh(Grid),
    {noreply, State#state{cellmap = NewCellmap}}.

%% Callbacks handled as normal gen_server callbacks
handle_info(_Msg, State) ->
    {noreply, State}.

handle_call(shutdown, _From, State=#state{parent=Panel}) ->
    wxPanel:destroy(Panel),
    {stop, normal, ok, State};

handle_call(_Msg, _From, State) ->
    {reply,{error, nyi}, State}.

handle_cast(Msg, State) ->
    io:format("Got cast ~p~n",[Msg]),
    {noreply,State}.

code_change(_, _, State) ->
    {stop, ignore, State}.

terminate(_Reason, _State) ->
    ok.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Local functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_grid(Panel, NumRows, NumCols) ->
    Grid = wxGrid:new(Panel, 2, []),
    wxGrid:createGrid(Grid, NumRows, NumCols),
    wxGrid:setRowLabelSize(Grid, 0),
    wxGrid:setColLabelSize(Grid, 0),
    wxGrid:setRowMinimalAcceptableHeight(Grid, ?MIN_CELL_SIZE),
    wxGrid:setColMinimalAcceptableWidth(Grid, ?MIN_CELL_SIZE),
    wx:foreach(
        fun(Row) ->
            wxGrid:setRowSize(Grid, Row, ?MIN_CELL_SIZE)
        end, lists:seq(0, NumRows)),

    wx:foreach(
        fun(Col) ->
            wxGrid:setColSize(Grid, Col, ?MIN_CELL_SIZE)
        end, lists:seq(0, NumCols)),

    wx:foreach(
        fun(Row) ->
            wx:foreach(
                fun(Col) ->
                    wxGrid:setReadOnly(Grid, Row, Col, [{isReadOnly, true}])
                end, lists:seq(0, NumCols))
        end, lists:seq(0, NumRows)),

    wxGrid:disableDragRowSize(Grid),
    wxGrid:disableDragColSize(Grid),

    wxGrid:connect(Grid, grid_cell_left_click),
    Grid.
